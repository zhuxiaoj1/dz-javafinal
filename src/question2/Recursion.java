package question2;

import java.util.ArrayList;

public class Recursion {
	public static void main(String[] args) {
		String[] strArr = { "test", "what", "who", "high", "low", "throw", "thing" };
		System.out.println(recursiveCount(strArr, 1));
	}

	public static int recursiveCount(String[] words, int n) {
		int count = 0, i = 0;
		return recursion(words, n, count, i);
	}
	
	public static int recursion(String[] words, int n, int count, int i) {
		if (!(i < words.length)) {
			return count;
		}
		
		if ((i % 2 != 0 && i >= n) || words[i].contains("w")) {
			count++;
		}
		i++;
		
		return recursion(words, n, count, i);
	}
}
