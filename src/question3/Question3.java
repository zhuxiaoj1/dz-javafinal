package question3;

import java.util.*;

import javafx.application.*;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;

/**
 * Class for question3
 * 
 * @author Danilo Zhu 1943382
 */
public class Question3 extends Application {
	private int money = 250; // The player's money
	Label outLabel, moneyLabel; // outLable: outcome of the bet, moneyLabel: displays the player's money
	TextField betTf; // The TextField where the player inputs their bet.
	Random ra = new Random();
	int roll = 0;

	@Override
	public void start(Stage root) throws Exception {
		root.setTitle("Question 3");

		GridPane fields = new GridPane();

		moneyLabel = new Label("Your money: " + money + "$");
		outLabel = new Label("");

		Label betLabel = new Label("Bet amount:");
		betTf = new TextField();
		betTf.setPromptText("Enter your bet amount");

		fields.add(moneyLabel, 0, 0);
		fields.add(betLabel, 1, 1);
		fields.add(betTf, 2, 1);

		fields.setHgap(10);
		fields.setVgap(10);

		Button evenBtn = assignEvent("Even");
		Button oddBtn = assignEvent("Odd");

		fields.add(evenBtn, 1, 2);
		fields.add(oddBtn, 2, 2);
		fields.add(outLabel, 1, 3);

		HBox container = new HBox(fields);
		container.setMinHeight(300);
		container.setMinWidth(500);
		Scene nScene = new Scene(container);
		root.setScene(nScene);
		root.show();
	}

	public static void main(String[] args) {
		launch(args); // Launching the application
	}

	/**
	 * This method assigns an event to occur once something happens to a button
	 * (such as a press).
	 * 
	 * @param betType This String represents the bet type to assign to the button
	 *                either "Even" or "Odd".
	 * @return A Button object, depending on betType.
	 */
	public Button assignEvent(String betType) {
		Button btn = new Button("Bet " + betType);
		btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

				if (betTf.getText().equals("")) {
					outLabel.setText("You didn't enter a bet value!");
				}
				int betMoney = Integer.parseInt(betTf.getText()); // Turning String input to int
				if (betMoney > money) {
					outLabel.setText("You don't have enough money!");
				} else {
					roll = ra.nextInt(7) + 1;
					if (betType.equals("Odd")) { // Sets conditions for Odd button
						if (roll % 2 != 0) {
							money += betMoney;
							moneyLabel.setText("Your money: " + money + "$");
							outLabel.setText("[" + roll + "] " + "Win: +" + betMoney);
						} else {
							money -= betMoney;
							moneyLabel.setText("Your money: " + money + "$");
							outLabel.setText("[" + roll + "] " + "Loss: -" + betMoney);
						}
					} else { // Sets conditions for Even button
						if (roll % 2 == 0) { 
							money += betMoney;
							moneyLabel.setText("Your money: " + money + "$");
							outLabel.setText("[" + roll + "] " + "Win: +" + betMoney);
						} else {
							money -= betMoney;
							moneyLabel.setText("Your money: " + money + "$");
							outLabel.setText("[" + roll + "] " + "Loss: -" + betMoney);
						}
					}
				}
			}
		});

		return btn;
	}
}
